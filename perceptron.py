"""
Contains several different variants of the Perceptron algorithm.
"""

from copy import deepcopy
import numpy as np
import random
import os


class PerceptronClassifier(object):
    """
    The base class for a number of Perceptron variants.
    """

    def __init__(self):
        """ Constructor """
        self.weight_vector = None

    def train(self, features, labels, rate=0.01, max_epochs=10):
        """
        Trains a Perceptron learner with a matrix of training data and a vector
        of labels that classify the training data.

        features:       The matrix of training data.
        labels:         The vector of labels that classify the training data.
        max_epochs:     The maximum number of epochs to run through.
        """
        sample_inds = range(features.shape[0])
        self.weight_vector = np.zeros(features.shape[1])
        for epoch in xrange(max_epochs):
            # Shuffle the data
            random.shuffle(sample_inds)
            for i in xrange(len(sample_inds)):
                # Get the feature vector and the actual label at the specified
                # index.
                x_i = features[sample_inds[i]]  # vector
                y_i = int(deepcopy(labels[sample_inds[i]]))
                if 0 == y_i:
                    y_i = -1

                # Use the current weight vector to predict the label.
                y_prime = int(self.predict(x_i))
                if 0 == y_prime:
                    y_prime = -1

                # Update the weight vector if we made a mistake.
                if y_i != y_prime:
                    self.weight_vector = self.weight_vector + (
                        rate * y_i * features[sample_inds[i]])

    def predict(self, feature_vec):
        """
        Predicts the label of a given feature vector based on the weight vector
        learned by the classifier.

        Arguments:
            feature_vec:    The vector of features for which we want to predict
                            a label.
        
        Returns:
            The label predicted by the classifier.
        """
        mult = np.dot(self.weight_vector, feature_vec)
        sign = np.sign(mult)
        if -1 == sign:
            sign = 0

        return int(sign)

    def accuracy_score(self, features, labels):
        """
        Reports the accuracy as a floating point value between zero (0%) and
        one (100%).

        Arguments:
            features:   The matrix of features to test.
            labels:     The actual labels that classify the input features.
        
        Returns:
            The accuracy score of the classifier's learned weight vector.
        """
        denominator = features.shape[0]
        numerator = 0
        for i in xrange(denominator):
            pred = self.predict(features[i])

            # Mark as correct if the prediction equals the actual label.
            if labels[i] == pred:
                numerator += 1

        # Compute the ratio.
        return float(numerator) / float(denominator)


class VotedPerceptronClassifier(PerceptronClassifier):
    """
    A class implementing a voted variant of the Perceptron algorithm.
    """

    def __init__(self):
        """ Constructor """
        super(VotedPerceptronClassifier, self).__init__()
        self.vec_count_list = []

    def train(self, features, labels, rate=0.01, max_epochs=10):
        """
        Trains a Voted Perceptron learner based on a matrix of feature data and
        their corresponding labels.

        Arguments:
            features:       The matrix of feature data to train on.
            labels:         The labels that classify the training data.
            rate:           The learning rate of the classifier.
            max_epochs:     The maximum number of times to run through the
                            training data.
        """
        sample_inds = range(features.shape[0])
        self.weight_vector = np.zeros(features.shape[1])
        curr_vec_index = 0
        self.vec_count_list.append([deepcopy(self.weight_vector), 1])
        for epoch in xrange(max_epochs):
            # Shuffle the data
            random.shuffle(sample_inds)
            for i in xrange(len(sample_inds)):
                # Get the feature vector and the actual label at the specified
                # index.
                x_i = features[sample_inds[i]]  # vector
                y_i = int(deepcopy(labels[sample_inds[i]]))
                if 0 == y_i:
                    y_i = -1

                # Use the current weight vector to predict the label.
                y_prime = int(
                    super(VotedPerceptronClassifier, self).predict(x_i))
                if 0 == y_prime:
                    y_prime = -1

                # If we make a mistake, update the weight vector and add it to
                # the list.
                if y_i != y_prime:
                    self.weight_vector = self.weight_vector + (
                        rate * y_i * features[sample_inds[i]])
                    self.vec_count_list.append(
                        [deepcopy(self.weight_vector), 1])
                    curr_vec_index += 1

                # If we didn't make a mistake, increment the number of votes for
                # the current weight vector.
                else:
                    self.vec_count_list[curr_vec_index][1] += 1

    def predict(self, feature_vec):
        """
        Predicts the label for a given feature vector based on a learned Voted
        Perceptron weight vector.

        Arguments:
            feature_vec:    The feature vector to classify.
        """
        running_sum = 0
        for k in xrange(len(self.vec_count_list)):
            # Since this prediction is based on sign, we need to use labels of
            # of -1 rather than 0.
            pred = int(np.sign(np.dot(self.vec_count_list[k][0], feature_vec)))
            if 0 == pred:
                pred = int(-1)

            # Multiply the number of votes that this weight vector got by its
            # prediction value and add it to the sum.
            running_sum += self.vec_count_list[k][1] * pred

        # Convert back to 0 label instead of -1, if needed, to match the data.
        sgn = int(np.sign(running_sum))
        if -1 == sgn:
            sgn = 0

        return sgn

    def write_vec_list_to_file(self, filename):
        """
        Writes the list of distinct weight vectors to a comma separated file.

        Arguments:
            filename:   The name of the file we want to create.
        """
        # First, check for an existing file with this name. If there is one,
        # delete it.
        try:
            os.remove(filename)
        except:
            # If there's not a file with this name already, there is no problem.
            pass

        with open(filename, 'w') as outfile:
            count_sum = 0
            for pair in self.vec_count_list:
                vec = pair[0]
                count = pair[1]
                count_sum += count

                vec_str = ""
                for i in xrange(len(vec)):
                    vec_str += str(vec[i])
                    if i != len(vec) - 1:
                        vec_str += ", "

                line = "Vector:, {}, Count:, {}\n".format(vec_str, count)
                outfile.write(line)

            last_line = "Num of distinct vectors:, {}\nSum of Counts:, {}\n".format(
                len(self.vec_count_list), count_sum)
            outfile.write(last_line)


class AveragedPerceptronClassifier(PerceptronClassifier):
    """
    A class implementing an averaged variant of the Perceptron algorithm.
    """

    def __init__(self):
        """ Constructor """
        super(AveragedPerceptronClassifier, self).__init__()
        self.ave_vector = None

    def train(self, features, labels, rate=0.01, max_epochs=10):
        """
        Trains an Averaged Perceptron classifier based on a matrix of training
        features and their corresponding labels.

        Arguments:
            features:       The matrix of training features.
            labels:         The vector of labels that classify the training data.
            rate:           The learning rate of the classifier.
            max_epochs:     The maximum number of times the classifier should
                            run through the training data.
        """
        sample_inds = range(features.shape[0])
        self.weight_vector = np.zeros(features.shape[1])
        self.ave_vector = np.zeros(features.shape[1])
        curr_vec_count = 1
        for k in xrange(max_epochs):
            # Shuffle the data
            random.shuffle(sample_inds)
            for i in xrange(len(sample_inds)):
                # Get the feature vector and the actual label at the specified
                # index.
                x_i = features[sample_inds[i]]  # vector
                y_i = int(deepcopy(labels[sample_inds[i]]))
                if 0 == y_i:
                    y_i = -1

                # Use the current weight vector to predict the label.
                y_prime = int(
                    super(AveragedPerceptronClassifier, self).predict(x_i))
                if 0 == y_prime:
                    y_prime = -1

                # If we make a mistake, add the old weight vector (with weight)
                # to the averaged vector.
                if y_i != y_prime:
                    self.ave_vector = self.ave_vector + (float(curr_vec_count)
                                                         * self.weight_vector)
                    self.weight_vector = self.weight_vector + (
                        rate * y_i * features[sample_inds[i]])
                    curr_vec_count = 1  # Reset current vector count
                else:
                    # If we are on the last iteration of the last epoch and we
                    # didn't make a mistake, we need to make sure we still
                    # update the averaged weight vector.
                    if (i == (len(sample_inds) - 1)) and (k ==
                                                          (max_epochs - 1)):
                        self.ave_vector = self.ave_vector + (
                            float(curr_vec_count) * self.weight_vector)

                    # Otherwise, increment the number of times we'll add this
                    # vector to the averaged weight vector.
                    else:
                        curr_vec_count += 1

    def predict(self, feature_vec):
        """
        Predicts the label of the feature vector based on a learned Averaged
        Perceptron weight vector.

        Arguments:
            feature_vec:    The feature vector to classify.
        
        Returns:
            The predicted label of the feature vector.
        """
        mult = np.dot(self.ave_vector, feature_vec)
        sign = np.sign(mult)
        if -1 == sign:
            sign = 0

        return int(sign)
