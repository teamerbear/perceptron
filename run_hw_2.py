"""
Runs the Homework 2 Perceptron computations.
"""

from extractor import extractor
from perceptron import PerceptronClassifier, VotedPerceptronClassifier, AveragedPerceptronClassifier

if __name__ == "__main__":
    # Extract training and test data.
    features_train, labels_train = extractor.extract(
        "data/percep_train.csv", append_bias=True)
    features_test, labels_test = extractor.extract(
        "data/percep_test.csv", append_bias=True)

    clf = PerceptronClassifier()
    r = 0.01
    print("\nTraining standard Perceptron classifier . . .")
    clf.train(features_train, labels_train, rate=r)
    print("\tLearned weight vector: {}\n".format(clf.weight_vector))
    print("Classifying test data . . .")
    train_acc = clf.accuracy_score(features_train, labels_train)
    test_acc = clf.accuracy_score(features_test, labels_test)
    print("\tTraining Accuracy: {}\n\tTest Accuracy: {}\n".format(train_acc,
                                                                  test_acc))

    vote_clf = VotedPerceptronClassifier()
    vote_r = 0.01
    print("\nTraining Voted Perceptron classifier . . .")
    vote_clf.train(features_train, labels_train, rate=vote_r)
    list_file = "vote_vec_list.csv"
    vote_clf.write_vec_list_to_file(list_file)
    print("\nWrote the list of weight vectors to {}".format(list_file))
    print("\nClassifying test data . . .")
    vote_train_acc = vote_clf.accuracy_score(features_train, labels_train)
    vote_test_acc = vote_clf.accuracy_score(features_test, labels_test)
    print("\tTraining Accuracy: {}\n\tTest Accuracy: {}\n".format(
        vote_train_acc, vote_test_acc))

    ave_clf = AveragedPerceptronClassifier()
    ave_r = 0.01
    print("\nTraining Averaged Perceptron classifier . . .")
    ave_clf.train(features_train, labels_train, rate=ave_r)
    print("\tLearned weight vector: {}\n".format(ave_clf.ave_vector))
    print("Classifying test data . . .")
    ave_train_acc = ave_clf.accuracy_score(features_train, labels_train)
    ave_test_acc = ave_clf.accuracy_score(features_test, labels_test)
    print("\tTraining Accuracy: {}\n\tTest Accuracy: {}\n".format(
        ave_train_acc, ave_test_acc))
